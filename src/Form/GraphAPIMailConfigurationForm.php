<?php

namespace Drupal\ms_graphapi_mail\Form;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Microsoft Graph API mail configuration form.
 */
class GraphAPIMailConfigurationForm extends ConfigFormBase {

  /**
   * Configuration name.
   */
  const CONFIG_NAME = 'ms_graphapi_mail.settings';

  /**
   * Form ID.
   */
  const FORM_ID = 'ms_graphapi_mail_graph_configuration_form';

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Class Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler interface.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigFormBase|GraphAPIMailConfigurationForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    global $config;
    $mailConfig = $this->config(self::CONFIG_NAME);
    // If the 'key' module exists then the settings might come from that module.
    $keyModuleEnabled = $this->moduleHandler->moduleExists('key');
    // Check if the required fields are overridden or not via the configuration
    // override system.
    $overriddenConfig = [];
    if (isset($config[self::CONFIG_NAME]) && !empty($config[self::CONFIG_NAME])) {
      $overriddenConfig = $config[self::CONFIG_NAME];
      $form['override_notice'] = [
        '#markup' => $this->t('You seem to have used the <a href=":config-override" target="_blank">Configuration Override System</a> to override configuration variables. As a result, you will not able to modify the read-only fields via the UI.', [
          ':config-override' => 'https://www.drupal.org/docs/drupal-apis/configuration-api/configuration-override-system',
        ]),
      ];
    }

    $form['authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication'),
    ];
    $form['authentication']['client_id'] = [
      '#type' => ($keyModuleEnabled === TRUE ? 'key_select' : 'textfield'),
      '#required' => !isset($overriddenConfig['client_id']),
      '#default_value' => $mailConfig->get('client_id'),
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('The application ID that the registration <a href=":portal" target="_blank">portal</a> assigned your app.', [
        ':portal' => 'https://go.microsoft.com/fwlink/?linkid=2083908',
      ]),
      '#disabled' => isset($overriddenConfig['client_id']),
    ];
    $form['authentication']['client_secret'] = [
      '#type' => ($keyModuleEnabled === TRUE ? 'key_select' : 'textfield'),
      '#required' => !isset($overriddenConfig['client_secret']),
      '#default_value' => $mailConfig->get('client_secret'),
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('The client secret that you created in the app registration portal for your app.'),
      '#disabled' => isset($overriddenConfig['client_secret']),
    ];
    $form['authentication']['tenant'] = [
      '#type' => 'textfield',
      '#required' => !isset($overriddenConfig['tenant']),
      '#default_value' => $mailConfig->get('tenant'),
      '#title' => $this->t('Tenant'),
      '#description' => $this->t('The <code>{tenant}</code> value in the path of the request can be used to control who can sign in to the application.'),
      '#disabled' => isset($overriddenConfig['tenant']),
    ];
    $form['authentication']['scope'] = [
      '#type' => 'textfield',
      '#default_value' => $mailConfig->get('scope'),
      '#title' => $this->t('Scope'),
      '#description' => $this->t('A space-separated list of scopes. If nothing is specified, then <code>.default</code> scope is applied.'),
      '#disabled' => isset($overriddenConfig['scope']),
    ];

    $form['mail_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail Configuration'),
    ];
    // @todo Change this to config entity to support multiple senders.
    $form['mail_configuration']['default_sender'] = [
      '#type' => 'textfield',
      '#required' => !isset($overriddenConfig['default_sender']),
      '#default_value' => $mailConfig->get('default_sender'),
      '#title' => $this->t('Default Sender UUID'),
      '#description' => $this->t('The default sender for the mail. As of now it is in version 4 format.'),
      '#disabled' => isset($overriddenConfig['default_sender']),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Some extra validation.
    if (!empty($form_state->getValue('default_sender')) && !Uuid::isValid($form_state->getValue('default_sender'))) {
      $form_state->setErrorByName('default_sender', $this->t('Default sender UUID should be a valid Version 4 UUID format.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(self::CONFIG_NAME)
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('tenant', $form_state->getValue('tenant'))
      ->set('scope', $form_state->getValue('scope'))
      ->set('default_sender', $form_state->getValue('default_sender'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
