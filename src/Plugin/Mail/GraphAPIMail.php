<?php

namespace Drupal\ms_graphapi_mail\Plugin\Mail;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Microsoft\Graph\Exception\GraphException;
use Microsoft\Graph\Graph as GraphAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ms_graphapi_mail\Service\GraphAPIAuthTokenProvider;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * GraphAPI mail class.
 *
 * @Mail(
 *   id = "ms_graphapi_mail_graphapi_mail",
 *   label = @Translation("Microsoft Graph API Mail Delivery"),
 *   description = @Translation("Sends the message using Microsoft Graph API.")
 * )
 */
class GraphAPIMail implements MailInterface, ContainerFactoryPluginInterface {

  /**
   * Configuration name.
   */
  const CONFIG_NAME = 'ms_graphapi_mail.settings';

  /**
   * Email address filter pattern.
   */
  const GRAPH_API_EMAIL_FILTER_REGEX = '/^\s*"?(.+?)"?\s*<\s*([^>]+)\s*>$/';

  /**
   * The graph api attachment data type.
   */
  const GRAPH_API_ATTACHMENT_DATA_TYPE = '#microsoft.graph.fileAttachment';

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logger service for the sendgrid_integration module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Graph API token provider instance.
   *
   * @var \Drupal\ms_graphapi_mail\Service\GraphAPIAuthTokenProvider
   */
  protected GraphAPIAuthTokenProvider $graphAPIAccessTokenProvider;

  /**
   * The file system instance.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('ms_graphapi_mail.token_provider'),
      $container->get('file_system')
    );
  }

  /**
   * Class constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration instance.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger instance.
   * @param \Drupal\ms_graphapi_mail\Service\GraphAPIAuthTokenProvider $ms_graphapi_mail_token_provider
   *   Graph API token provider instance.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Drupal file system instance.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    GraphAPIAuthTokenProvider $ms_graphapi_mail_token_provider,
    FileSystemInterface $file_system
  ) {
    $this->configFactory = $configFactory;
    $this->logger = $loggerChannelFactory->get('ms_graphapi_mail');
    $this->graphAPIAccessTokenProvider = $ms_graphapi_mail_token_provider;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritDoc}
   */
  public function format(array $message): array {
    // Join message array.
    $message['body'] = implode("\n\n", $message['body']);
    return $message;
  }

  /**
   * {@inheritDoc}
   */
  public function mail(array $message) {
    try {
      $token = $this->graphAPIAccessTokenProvider->generateBearerToken();
      if (!empty($token)) {
        $graph = new GraphAPI();
        $graph->setAccessToken($token);
        // Create the mail body.
        $mailObject = [
          'message' => [
            'subject' => $message['subject'],
            // 'from' => $this->addFrom($message['from'])['email'], // no need.
            'toRecipients' => $this->addRecipients($message['to']),
            'body' => [
              'contentType' => ($this->isHtml($message) === TRUE ? 'HTML' : 'Text'),
              'content' => $message['body'],
            ],
          ],
        ];
        // Format any attachment present.
        if (isset($message['params']['attachments']) && !empty($message['params']['attachments'])) {
          $mailObject['message']['attachments'] = $this->addAttachments($message['params']['attachments']);
        }
        $defaultSenderUUID = $this->configFactory->get(self::CONFIG_NAME)->get('default_sender');
        try {
          $request = $graph->createRequest('POST', '/users/' . $defaultSenderUUID . '/sendmail')
            ->attachBody($mailObject)
            ->execute();
          if ($request->getStatus() == 202) {
            return TRUE;
          }
        }
        catch (GraphException $ge) {
          $this->logger->error('Error to send mail: ' . $ge->getMessage());
          return FALSE;
        }
      }
    }
    catch (EntityStorageException $e) {
      $this->logger->error('Error to generate the token: ' . $e->getMessage());
      return FALSE;
    }
  }

  /**
   * Sets the sender email address to the Mail object.
   *
   * @param string $form
   *   The from addresses.
   */
  protected function addFrom(string $form = '') {
    if (!empty($message['from'])) {
      $address_from = $this->parseAddress($message['from']);
    }
    else {
      $address_from = [
        'email' => $this->configFactory->get('system.site')->get('mail'),
        'name' => $this->configFactory->get('system.site')->get('name'),
      ];
    }

    return $address_from;
  }

  /**
   * Method adds receipients to the mail object.
   *
   * @param string $to
   *   The string containing a comma separated mail addresses.
   *
   * @return array
   *   Array containing the formatted address which converts to JSON later.
   */
  protected function addRecipients(string $to) {
    $toAddress = [];
    if (strpos($to, ',')) {
      $multiple = explode(',', $to);
    }
    else {
      $toAddress[] = [
        'emailAddress' => [
          'address' => $to,
        ],
      ];
    }
    return $toAddress;
  }

  /**
   * Method parses email addresses.
   *
   * @param string $email
   *   A string containing all email.
   *
   * @return array
   *   Containing the matches.
   */
  private function parseAddress(string $email): array {
    if (preg_match(self::GRAPH_API_EMAIL_FILTER_REGEX, $email, $matches)) {
      return [$matches[2], strval($matches[1])];
    }
    else {
      return [$email];
    }
  }

  /**
   * Checks if the mail is HTML or text.
   *
   * @param array $message
   *   The Drupal message array.
   *
   * @return bool
   *   If HTML then TRUE or FALSE.
   */
  protected function isHtml(array $message): bool {
    if (isset($message['params']['html']) && $message['params']['html'] === TRUE) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Method adds attachment to the mail object.
   *
   * @param array $attachments
   *   Array containing all the attachments.
   *
   * @return array
   *   Array formatted with attachments.
   */
  protected function addAttachments(array $attachments): array {
    $files = [];
    // @todo Fix mime mail support.
    foreach ($attachments as $attachment) {
      // Mime mail is not a hard dependency for this module.
      // So support others as well.
      if (isset($attachment['filecontent'])) {
        $files[] = [
          '@odata.type' => self::GRAPH_API_ATTACHMENT_DATA_TYPE,
          'name' => $attachment['filename'],
          'contentType' => $attachment['filemime'],
          'contentBytes' => base64_encode($attachment['filecontent']),
        ];
      }
    }

    return $files;
  }

}
