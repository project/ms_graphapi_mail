<?php

namespace Drupal\ms_graphapi_mail\Service;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Authentication token provider and manager.
 */
class GraphAPIAuthTokenProvider {

  /**
   * Config name.
   */
  const CONFIG_NAME = 'ms_graphapi_mail.settings';

  /**
   * The API base URL.
   */
  const GRAPH_API_BASE_URL = 'https://login.microsoftonline.com';

  /**
   * The Token Endpoint.
   */
  const GRAPH_API_TOKEN_ENDPOINT = 'oauth2/v2.0/token';

  /**
   * The name of the state variable which stores the bearer validity time.
   */
  const GRAPH_API_BEARER_VALIDITY_STATE_VAR = 'ms_graphapi_mail_bearer_validity';

  /**
   * The name of the state variable which stores the bearer token.
   */
  const GRAPH_API_BEARER_STORAGE_STATE_VAR = 'ms_graphapi_mail_bearer_value';

  /**
   * Configuration factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Client interface instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Module handler object.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerChannelFactory;

  /**
   * The key repository instance.
   *
   * @var \Drupal\key\KeyRepositoryInterface|null
   */
  protected ?KeyRepositoryInterface $keyRepository;

  /**
   * State instance.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The time instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $time;

  /**
   * Class Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory instance.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   GuzzleHTTP client instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler instance.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Default logger factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   State interface.
   * @param \Drupal\Component\Datetime\Time $datetime_time
   *   Time instance.
   * @param \Drupal\key\KeyRepositoryInterface|null $key_repository
   *   Optional key repository.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    ModuleHandlerInterface $module_handler,
    LoggerChannelFactoryInterface $logger_factory,
    StateInterface $state,
    Time $datetime_time,
    KeyRepositoryInterface $key_repository = NULL
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->moduleHandler = $module_handler;
    $this->loggerChannelFactory = $logger_factory;
    $this->state = $state;
    $this->time = $datetime_time;
    $this->keyRepository = $key_repository;
  }

  /**
   * Generate and store the token in the proper location and return.
   *
   * @return string|null
   *   String containing the Bearer token.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateBearerToken():?string {
    $bearer = '';
    if (TRUE === $this->isTokenValid()) {
      // Return from the storage.
      $bearer = $this->getBearerToken();
    }
    else {
      // Generate a new token.
      $bearer = $this->createNewBearerToken();
    }
    return $bearer;
  }

  /**
   * A special method - client id can also be stored inside Key Repository.
   *
   * @return string
   *   Contains the client id.
   */
  public function getClientId(): string {
    $clientIdFromKey = $this->getValueFromKeyStorage('client_id');
    return !empty($clientIdFromKey) ? $clientIdFromKey : $this->getConfig()->get('client_id');
  }

  /**
   * A special method - client secret can also be stored inside Key Repository.
   *
   * @return string
   *   Contains the client secret.
   */
  public function getClientSecret(): string {
    $clientSecretFromKey = $this->getValueFromKeyStorage('client_secret');
    return !empty($clientSecretFromKey) ? $clientSecretFromKey : $this->getConfig()->get('client_secret');
  }

  /**
   * Get the Scope.
   *
   * @return string
   *   A string containing the scope from configuration or the default scope.
   */
  public function getScope(): string {
    $configScope = $this->getConfig()->get('scope');
    if (empty($configScope)) {
      return '.default';
    }
  }

  /**
   * Method checks if the token is valid or not.
   *
   * @return bool
   *   true if the token is still valid else false.
   */
  protected function isTokenValid():bool {
    $requestTime = $this->time->getRequestTime();
    $savedExpiryTime = $this->state->get(self::GRAPH_API_BEARER_VALIDITY_STATE_VAR);
    if (!empty($savedExpiryTime) && $requestTime < $savedExpiryTime) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Set the state variable for Bearer expiry.
   *
   * @param int $interval
   *   The interval in seconds (integer).
   */
  protected function setBearerTokenExpiry(int $interval):void {
    $time = $this->time->getRequestTime() + $interval;
    $this->state->set(self::GRAPH_API_BEARER_VALIDITY_STATE_VAR, $time);
  }

  /**
   * Method returns the saved bearer token.
   *
   * @return string
   *   The token as string.
   */
  protected function getBearerToken():string {
    if ($this->moduleHandler->moduleExists('key')) {
      $entity = $this->keyRepository->getKey('graph_api_bearer_token');
      return $entity->getKeyValue();
    }
    else {
      return $this->state->get(self::GRAPH_API_BEARER_STORAGE_STATE_VAR);
    }
  }

  /**
   * Method to save the bearer token.
   *
   * @param string $token
   *   The bearer token.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setBearerToken(string $token):void {
    // Check if 'key' module is enabled or not.
    // If key module is enabled, then store in the 'graph_api_bearer_token'
    // configuration else store it in a state variable.
    // Storing the value in state variable is not recommended.
    if ($this->moduleHandler->moduleExists('key')) {
      $entity = $this->keyRepository->getKey('graph_api_bearer_token');
      if ($entity instanceof KeyInterface) {
        $entity->setKeyValue($token);
        $entity->save();
      }
      else {
        // This should not happen but in case, log an error.
        $this->loggerChannelFactory
          ->get('ms_graphapi_mail')
          ->error('The required key - graph_api_bearer_token not found.');
      }
    }
    else {
      // Without Key module, save inside a state variable.
      $this->state->set(self::GRAPH_API_BEARER_STORAGE_STATE_VAR, $token);
    }
  }

  /**
   * Method creates a new bearer token.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createNewBearerToken():?string {
    $config = $this->getConfig();
    // Check bearer token expiry.
    $uri = sprintf("%s/%s/%s", self::GRAPH_API_BASE_URL, $config->get('tenant'), self::GRAPH_API_TOKEN_ENDPOINT);
    try {
      $loginRequest = $this->httpClient->request('POST', $uri, [
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => $this->getClientId(),
          'client_secret' => $this->getClientSecret(),
          'scope' => $this->getScope(),
        ],
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
      ]);
      if ($loginRequest->getStatusCode() == 200) {
        $loginResponse = $loginRequest->getBody()->getContents();
        if ($loginData = json_decode($loginResponse, TRUE)) {
          if (isset($loginData['expires_in'])) {
            $this->setBearerTokenExpiry((int) $loginData['expires_in']);
          }
          if (isset($loginData['access_token'])) {
            $this->setBearerToken($loginData['access_token']);
            return $loginData['access_token'];
          }
        }
      }
    }
    catch (GuzzleException $e) {
      $this->loggerChannelFactory->get('ms_graphapi_mail')->error('Error while generating the bearer token - ' . $e->getMessage());
      return NULL;
    }
  }

  /**
   * Function returns the configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Configuration object.
   */
  private function getConfig(): ImmutableConfig {
    return $this->configFactory->get(self::CONFIG_NAME);
  }

  /**
   * Check key module and get value from key repository.
   *
   * @param string $key
   *   The key to search for.
   *
   * @return string|null
   *   Either the item value or null.
   */
  private function getValueFromKeyStorage(string $key):? string {
    $value = NULL;
    if ($this->moduleHandler->moduleExists('key')) {
      // The config might have been stored in Key repository.
      $configValue = $this->getConfig()->get($key);
      $clientIdKeyEntity = $this->keyRepository->getKey($configValue);
      if (!empty($clientIdKeyEntity)) {
        $value = $clientIdKeyEntity->getKeyValue();
      }
    }
    return $value;
  }

}
